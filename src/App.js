import React from 'react';
import {
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import './App.sass';
import About from './views/landing/about/AboutMe.js';
import Examples from './views/examples/Menu.js';

function App() {
  return (
    <div id="main">
      <nav>
        <ul>
          <li>
            <NavLink exact to="/">Home</NavLink>
          </li>
          <li>
            <NavLink to="/examples">Examples</NavLink>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route path="/examples">
          <Examples />
        </Route>
        <Route path="/">
          <About />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
