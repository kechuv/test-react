import { useState, useEffect } from 'react';

function useClickState(initialValue) {
  const [count, setCount] = useState(initialValue);
  useEffect(() => {
    methods.updateTitle();
    return () => {
      methods.resetTitle();
    }
  });

  const methods = {
    updateTitle: () => {
      document.title = `You clicked ${count} times`;
    },
    resetTitle: () => {
      document.title = 'React App';
    }
  }

  return {
    count,
    onClick: () => { setCount(count + 1) }
  };
};

export default useClickState;