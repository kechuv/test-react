import React from 'react';
import useClickState from './useClickState.js';
import './Clicks.sass'

function Clicks({msg}) {
  const {count, onClick} = useClickState(0);

  return (
    <div>
      <p>{msg || 'Clicked'} {count} times!</p>
      <button onClick={onClick}>
        Click me!
      </button>
    </div>
  );
}

export default Clicks;