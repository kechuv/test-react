import { useState } from 'react';

function useTodoState(initialValue) {
  const [todos, setTodos] = useState(initialValue);

  const methods = {
    deleteTodo: (todoIndex) => {
      const newTodos = todos.filter((todo, index) => index !== todoIndex);
      setTodos(newTodos);
    },
    editTodo: (todoText, todoIndex) => {
      const newTodos = todos.map((todo, index) => index === todoIndex ? todoText : todo);
      setTodos(newTodos);
    }
  }

  return {
    todos,
    addTodo: (todoText) => { setTodos([...todos, todoText]); },
    editTodo: methods.editTodo,
    deleteTodo: methods.deleteTodo
  };
};

export default useTodoState;