import React from 'react';
import './TodoList.sass';
import TodoForm from '../form/TodoForm.js';

function TodoList({todos, saveTodo, deleteTodo}) {
  // const { }

  return (
    <ul className="todo-list">
      {todos.map((todo, index) => (
        <li key={ index }>
          <span>{ todo }</span>
          <TodoForm
          saveTodo={ saveTodo }
          todoIndex={ index } />
          <div className="actions">
            {/* <span className="link" onClick={() => }>Edit</span> */}
            <span className="link" onClick={() => deleteTodo(index)}>Delete</span>
          </div>
        </li>
      ))}
    </ul>
  )
}

export default TodoList;