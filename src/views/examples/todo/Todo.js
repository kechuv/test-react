import React from 'react';
import TodoForm from './form/TodoForm.js';
import TodoList from './list/TodoList.js';
import useTodoState from './useTodoState.js';
import './Todo.sass'

function Todo() {
  const { todos, addTodo, editTodo, deleteTodo } = useTodoState([]);

  const methods = {
    saveTodo: (todoText, todoIndex) => {
      const trimText = todoText.trim();
      if (trimText.length > 0) {
        if (todoIndex === undefined) addTodo(trimText)
        else editTodo(trimText, todoIndex)
      }
    }
  }

  return (
    <main id="todos">
      <div className="container">
        <h2>To do's</h2>
        <TodoForm
        saveTodo={ methods.saveTodo } />
        <TodoList
        todos={ todos }
        saveTodo={ methods.saveTodo }
        deleteTodo={ deleteTodo } />
      </div>
    </main>
  )
}

export default Todo;