import React from 'react';
import useFormState from './useFormState.js';
import './TodoForm.sass';

function TodoForm({saveTodo, todoIndex}) {
  const {value, onChange, reset} = useFormState('');

  return (
    <form
    className="todo-form"
    onSubmit={(event) => {
      event.preventDefault();
      saveTodo(value, todoIndex);
      reset();
    }}>
      <div className="input-container">
        <input
        type="text"
        name="todo"
        id="todo"
        placeholder="Add todo"
        onChange={onChange}
        value={value} />
        <i
        className="fas fa-plus"
        onClick={() => {
          saveTodo(value, todoIndex);
          reset();
        }}></i>
      </div>
    </form>
  )
}

export default TodoForm;