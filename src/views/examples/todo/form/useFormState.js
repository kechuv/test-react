import { useState } from 'react';

function useFormState(initialValue) {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    onChange: (event) => { setValue(event.target.value); },
    reset: () => { setValue(''); }
  };
};

export default useFormState;