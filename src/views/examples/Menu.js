import React from 'react';
import {
  Switch,
  Route,
  NavLink,
  useRouteMatch
} from "react-router-dom";
import './Menu.sass'
import Todo from './todo/Todo.js';
import Clicks from './clicks/Clicks.js';

function Menu() {
  const match = useRouteMatch();

  const backMenu = (
    <nav>
      <NavLink
      to="/examples">
        &lt; Go Back
      </NavLink>
    </nav>
  );
  const mainMenu = (
    <nav>
      <ol>
        <li>
          <NavLink
          to={`${match.url}/todo`}>
            'To do' example
          </NavLink>
        </li>
        <li>
          <NavLink
          to={`${match.url}/clicks`}>
            'Clicks' example
          </NavLink>
        </li>
      </ol>
    </nav>
  );

  return (
    <main id="examples" className="container">
      <Switch>
        <Route path={`${match.url}/clicks`}>
          {backMenu}
          <Clicks />
        </Route>
        <Route path={`${match.url}/todo`}>
          {backMenu}
          <Todo />
        </Route>
        <Route path={match.url}>
          {mainMenu}
          <div className="container">
            <h1>Select an example</h1>
          </div>
        </Route>
      </Switch>
    </main>
  )
}

export default Menu;