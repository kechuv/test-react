import React from 'react';
import './AboutMe.sass'

function AboutMe() {
  return (
    <main id="about" className="container">
      <article>
        <h1>Hi, I'm <a href="https://valencia-site.web.app" target="_blank" rel="noopener noreferrer">Kheops<i className="fas fa-external-link-alt"></i></a>! Front-end Web developer who can design.</h1>
        <p>It's been a long time since I last developed a website / webapp using frameworks other than Vue.js, so this site was built to demonstrate my progress re-learning how to React, now that React staff has introduced <a href="https://reactjs.org/docs/hooks-intro.html" target="_blank" rel="noopener noreferrer">Hooks</a> and gave functional components more power.</p>
        <p className="embed-gif">
          <img src="https://media.giphy.com/media/3o84sq21TxDH6PyYms/200w.webp" alt="more power"/>
        </p>
        <p>But don't worry, I personally still prefer Vue.js, I'm too used to it and I love it <span role="img" aria-label="FACE WITH STUCK-OUT TONGUE AND TIGHTLY-CLOSED EYES">&#128541;</span>.</p>
        <p>Anyway, grab some popcorn and enjoy how I break stuff here and if you'd like to check the code click <button className="link">here</button>.</p>
      </article>
    </main>
  );
}

export default AboutMe;